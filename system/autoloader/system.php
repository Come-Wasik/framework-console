<?php

spl_autoload_register(function ($className) {
    ## Define system directory ##
    $frameworkPath = base_path('system/framework/components');

    $autoloadContent = [
        'Framework\RouteSystem' => $frameworkPath . '/router',
        'Framework\Backpack' => $frameworkPath . '/backpack',
        'Framework\Interfaces' => $frameworkPath . '/interfaces',
        'Framework\CacheSystem' => $frameworkPath . '/cache'
    ];

    ### Extract namespace and classe name ###
    $namespaceParts = explode('\\', $className);
    $justClassName = array_pop($namespaceParts);
    $justNamespace = implode('\\', $namespaceParts);

    if (key_exists($justNamespace, $autoloadContent)) {
        $classPath = $autoloadContent[$justNamespace] . '/' . $justClassName . '.php';
        include $classPath;
    }
});
