<?php

# Include essential functions for bootstrap
require __DIR__ . '/ressources/functions.php';

# Define path
$autoloadDir = base_path('system/autoloader');
$helperDirectory = base_path('app/helper');

# Read autoloader files
foreach (scandir($autoloadDir) as $filename) {
    if (is_file($autoloadDir . '/' . $filename)) {
        require $autoloadDir . '/' . $filename;
    }
}
# Read helper directory
foreach (scandir($helperDirectory) as $filename) {
    if (is_file($helperDirectory . '/' . $filename)) {
        require $helperDirectory . '/' . $filename;
    }
}
