<?php

namespace Framework\CacheSystem;

use Exception;

class Manager
{
    public function set(string $name, string $content)
    {
        if (empty($name) || empty($content)) {
            throw new Exception('Cache name or content is empty', 500);
        }
        if ((strpos($name, '..')) !== false) {
            throw new Exception('Cannot go back', 500);
        }

        file_put_contents(storage_path($name), $content);
    }

    public function get(string $name)
    {
        if (empty($name)) {
            throw new Exception('Cache name or content is empty', 500);
        }
        if ((strpos($name, '..')) !== false) {
            throw new Exception('Cannot go back', 500);
        }

        return file_get_contents(storage_path($name));
    }

    public function has(string $name)
    {
        if (file_exists(storage_path($name))) {
            return true;
        }
        return false;
    }

    public function delete(string $name)
    {
        if (empty($name)) {
            throw new Exception('Cache name is empty', 500);
        }
        if ((strpos($name, '..')) !== false) {
            throw new Exception('Cannot go back', 500);
        }

        if (file_exists(storage_path($name))) {
            unlink(storage_path($name));
        }
    }
}
