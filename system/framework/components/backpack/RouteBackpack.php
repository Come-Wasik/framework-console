<?php

namespace Framework\Backpack;

use Framework\Interfaces\RouteBackpackInterface;
use Framework\RouteSystem\Route;

class RouteBackpack implements RouteBackpackInterface
{
    private static $backpack = [];

    public static function append(Route $content)
    {
        self::$backpack[] = $content;
    }

    public static function get(int $indice): ?Route
    {
        return self::$backpack[$indice] ?? null;
    }

    public static function all(): array
    {
        return self::$backpack;
    }
}
