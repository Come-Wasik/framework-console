<?php

namespace Framework\Interfaces;

interface IndicedBackpackInterface
{
    public static function append($content);

    public static function get(int $indice);

    public static function all(): array;
}
