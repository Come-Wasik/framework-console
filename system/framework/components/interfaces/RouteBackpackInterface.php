<?php

namespace Framework\Interfaces;

use Framework\RouteSystem\Route;

interface RouteBackpackInterface
{
    public static function append(Route $content);

    public static function get(int $indice): ?Route;

    public static function all(): array;
}
