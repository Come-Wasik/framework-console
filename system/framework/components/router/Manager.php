<?php

namespace Framework\RouteSystem;

use Exception;
use Framework\Backpack\RouteBackpack;

class Manager
{
    const OBLIGATION_SYMBOL = '+';
    const OPTIONNAL_SYMBOL = '-';

    /**
     * Addding route in the backpack
     *
     * @param Framework\RouteSystem\Route $route A route created
     */
    public function addRoute(Route $route): void
    {
        foreach (RouteBackpack::all() as $registeredRoute) {
            if ($route->getName() == $registeredRoute->getName()) {
                throw new Exception('Two routes cannot have the same name. Fix this route name : ' . $route->getName(), 500);
            }
        }
        RouteBackpack::append($route);
    }

    /**
     * Register a route file (just include the route file)
     *
     * @param string $routeFile A path to a file which call route add method
     */
    public function registerFromFile(string $routeFile): void
    {
        if (!file_exists($routeFile)) {
            throw new Exception('The route file does not exist', 500);
        }

        include $routeFile;
    }

    /**
     * Search to match a sended route with a route contained in the backpack
     *
     * @param string $commandNeeded The sended route
     * @param array $routeList The list of routes
     * @return array|null A route matched or null
     */
    public function searchThisRoute($commandNeeded): ?array
    {
        $args = [];

        if (strlen($commandNeeded) <= 0) {
            throw new Exception('Command empty', 404);
        }

        foreach (RouteBackpack::all() as $route) {
            ## Equality comparaison
            if ($commandNeeded == $route->getCommandPattern()) {
                return [
                    'route' => $route,
                    'args' => $args // An empty array at this state
                ];
            }

            ## Symbol comparaison
            $isEqual = true;

            $neededParts = explode(' ', $commandNeeded);

            # On vérifie que la commande contient plusieurs arguments
            if (count($neededParts) > 1) {
                $comparedParts = explode(' ', $route->getCommandPattern());

                # On vérifie que les 1ers arguments sont les mêmes
                if (
                    // count($comparedParts) > 1
                    // && $neededParts[0] == $comparedParts[0]
                    count($neededParts) <= count($comparedParts)
                ) {
                    # Recherche de symboles
                    for ($i = 0; $i < count($comparedParts) && $isEqual; $i++) {
                        if (
                            (
                                isset($neededParts[$i])
                                && (
                                    $neededParts[$i] == $comparedParts[$i]
                                    || (
                                        $comparedParts[$i] == self::OBLIGATION_SYMBOL
                                        && strlen($neededParts[$i]) > 0
                                    )
                                )
                            ) || (
                                $comparedParts[$i] == self::OPTIONNAL_SYMBOL
                            )
                        ) {
                        } else {
                            $isEqual = false;
                        }
                    }

                    if ($isEqual) {
                        # Register arguments
                        for ($i = 0; $i < count($comparedParts); $i++) {
                            if (
                                $comparedParts[$i] == self::OBLIGATION_SYMBOL
                                ||
                                (
                                    $comparedParts[$i] == self::OPTIONNAL_SYMBOL
                                    && isset($neededParts[$i])
                                )
                            ) {
                                $args[] = $neededParts[$i];
                            }
                        }

                        # Return route data
                        return [
                            'route' => $route,
                            'args' => $args
                        ];
                    }
                }
            }
        }
        return null;
    }
}
