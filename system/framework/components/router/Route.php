<?php

namespace Framework\RouteSystem;

use Exception;

class Route
{
    private $name;
    private $commandPattern;
    private $action;

    public function __construct(array $data = [])
    {
        foreach ($data as $varName => $value) {
            $method = 'set' . ucfirst($varName);
            if (
                method_exists($this, $method)
                ) {
                $this->$method($value);
            }
        }
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getCommandPattern(): ?string
    {
        return $this->commandPattern;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setCommandPattern(string $commandPattern): void
    {
        $this->commandPattern = $commandPattern;
    }

    public function setAction($action): void
    {
        if (
            !is_callable($action)
            && !is_string($action)
        ) {
            throw new Exception('The route action needs to be a string like "controller@method" or a callable', 500);
        }

        if (is_string($action)) {
            if (
                empty($action)
                || \substr_count($action, '@') != 1
                || \strpos($action, '@', 1) === false
                || \strlen($action) < 3
            ) {
                throw new Exception('The route action ' . $action . ' cannot be registered. It must be a valid "controller@method"', 500);
            }
        }

        $this->action = $action;
    }
}
