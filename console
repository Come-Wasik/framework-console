#!/usr/bin/env php
<?php

use Framework\RouteSystem\Manager as RouteManager;

require __DIR__ . '/system/framework/bootstrap/main.php';

### main starting ###
try {
    ## Get the user command
    $userCommand = ($argv = array_slice($_SERVER['argv'], 1)) || false ? implode(' ', $argv) : 'help';

    ## Search the command in the list
    $router = new RouteManager();
    $router->registerFromFile(base_path('routes/console.php'));
    $routeContainer = $router->searchThisRoute($userCommand);

    if ($routeContainer === null) {
        throw new Exception('Command not found', 404);
    }

    $route = $routeContainer['route'];
    $routeArgs = $routeContainer['args'];
    $routeAction = $route->getAction();

    ## Actions to do if the action is a callable
    if (is_callable($routeAction)) {
        $routeAction(...$routeArgs);
    }

    ## Actions to do if the action is a string
    if (is_string($routeAction)) {
        list($controller, $method) = explode('@', $routeAction);

        ## Access to Controller verification
        if (!class_exists($controller)) {
            throw new Exception('Class ' . $controller . ' don\'t exist', 1);
        }

        if (!method_exists($controller, $method)) {
            throw new Exception('Method ' . $method . ' don\'t exist', 1);
        }

        # Launch the method
        (new $controller)->$method(...$routeArgs);
    }
} catch (Exception $e) {
    write($e->getMessage(), 'error');
}
