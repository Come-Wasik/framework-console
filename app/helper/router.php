<?php

use Framework\RouteSystem\Manager;
use Framework\RouteSystem\Route;

if (!function_exists('newCommand')) {
    function newCommand(string $name, string $commandPattern, $action)
    {
        $manager = new Manager();
        $manager->addRoute(
            new Route([
                'name' => $name,
                'commandPattern' => $commandPattern,
                'action' => $action
            ])
        );
    }
}
