<?php

if (!function_exists('write')) {
    function write(string $content, string $state = 'normal')
    {
        switch ($state) {
                case 'success':
                    $color = '[92m';
                    break;
                case 'error':
                    $color = '[31m';
                    break;
                case 'info':
                    $color = '[93m';
                    break;
                case 'normal':
                default:
                    $color = '[39m';
                    break;
            }

        echo "\e" . $color . '[' . date('Y-m-d i:m:s') . '] ' . $content . "\e[0m\n";
    }
}

if (!function_exists('app_path')) {
    function app_path(string $nextPath = '')
    {
        return (!empty($nextPath) ? base_path('app' . '/' . $nextPath) : base_path('app'));
    }
}

if (!function_exists('storage_path')) {
    function storage_path(string $nextPath = '')
    {
        return (!empty($nextPath) ? base_path('storage' . '/' . $nextPath) : base_path('storage'));
    }
}
