<?php

namespace App\Controller;

class MakeController
{
    public function makeClass(string $className, string $classAttributes = null)
    {
        # Class directories #
        $classDir = app_path('component');

        # The class name should be in PascalCase
        $className = ucfirst($className);

        # Filtering of ; at the beginning and end so as not to write an empty value in the file later on
        $classAttributes = trim($classAttributes, ';"');

        $getAttrType = function ($attribute) {
            $attribute = trim($attribute, ',;:');
            if (count($temp = explode(':', $attribute)) == 2) {
                return [
                    $temp[0],
                    $temp[1]
                ];
            }
            return [
                $temp[0],
                null
            ];
        };

        $fileContent = "<?php\n"
            . "\n"
            . "namespace App\\Component;\n"
            . "\n"
            . 'class ' . $className . "\n"
            . "{\n";

        # Properties definition
        if ($classAttributes) {
            foreach (explode(';', $classAttributes) as $attribute) {
                $attribute = trim($attribute);
                list($attribute, $attrType) = $getAttrType($attribute);

                $fileContent .= "\tprivate $" . $attribute . ";\n";
            }

            # getters definition
            $fileContent .= "\n";
            foreach (explode(';', $classAttributes) as $attribute) {
                $attribute = trim($attribute);
                list($attribute, $attrType) = $getAttrType($attribute);

                $fileContent .= "\tpublic function get" . ucfirst($attribute) . '()' . ($attrType !== null ? ': ?' . $attrType : '') . "\n"
                    . "\t{\n"
                    . "\t\treturn \$this->" . $attribute . ";\n"
                    . "\t}\n";
            }

            # Setters definition
            $fileContent .= "\n";
            foreach (explode(';', $classAttributes) as $attribute) {
                $attribute = trim($attribute);
                list($attribute, $attrType) = $getAttrType($attribute);

                $fileContent .= "\tpublic function set" . ucfirst($attribute) . '(' . ($attrType !== null ? $attrType . ' ' : '') . '$' . $attribute . "): void\n"
                    . "\t{\n"
                    . "\t\t\$this->" . $attribute . ' = $' . $attribute . ";\n"
                    . "\t}\n";
            }
        } else {
            $fileContent .= "\t\n";
        }

        # End of file
        $fileContent .= "}\n";

        # Generate the directory if not exists #
        if (!file_exists($classDir)) {
            mkdir($classDir);
        }

        # Generate the file #
        file_put_contents($classDir . '/' . $className . '.php', $fileContent);
        write('The class ' . $className . ' has been created.', 'success');
    }
}
