<?php

use App\Controller\MakeController;
use App\Controller\MyController;

newCommand('hello', 'home', MyController::class . '@index');
newCommand('maker.class', 'make:class + -', MakeController::class . '@makeClass');
